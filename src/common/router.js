'use strict';

class Routing {

    /**
     * Handles an error response to the client.
     * Modify the data returned to show only relevant information.
     * Show debug info if the env is not production.
     * @param {Error} err Error object before it is returned to the client
     * @param {Object} req Restify request object
     * @param {Object} res Restify response object
     */
    static handleError (err, req, res) {
        if(err.params && err.params.token) {
            res.setHeader('DelubacToken', err.params.token);
        }
        res.send(400, err);
    }


    /**
     * Handles a succesful response to the client
     * @param {*} data Data returned to the client
     * @param {Object} req Restify request object
     * @param {Object} res Restify response object
     */
    static handleSuccess (data, req, res) {
        let response = data.response ? data.response : data;
        if(data && data.token) {
            res.setHeader('DelubacToken', data.token);
        }
        return res.send(200, response);
    }

    /**
     * Handles the response to the client
     * @param {Promise} promise Promise resolved or rejected
     * @param {Object} req Restify request object
     * @param {Object} res Restify response object
     */
    static handlePromise (promise, req, res) {
        return promise
            .then(function (data) {
                Routing.handleSuccess(data, req, res);
            })
            .catch(function (err) {
                Routing.handleError(err, req, res);
            });
    }

}

module.exports = Routing;

