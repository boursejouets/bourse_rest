'use strict';

const request   = require('request');
const rp        = require('request-promise');

/**
 * Surcharge le module request pour y ajouter une gestion du token client
 */
class HttpService {}

// ------------------------------------------------------------------------------------------

/**
 * Execute une requete HTTP
 * @param {String} callerId
 * @param {Object} options
 */
HttpService.prototype.httpRequest = function (callerId, options) {

    const jar = request.jar();

    if (this.auth.token) jar.setCookie(request.cookie('token=' + this.auth.token), options.url);

    const httpOptions = {
        method: options.method || 'GET',
        url: options.url,
        json: true,
        jar
    };

    if (options.body) httpOptions.body = options.body;
    if (options.headers) httpOptions.headers = options.headers;

    this.log.debug(this.wsName, '::', httpOptions.method, options.url);

    return rp(httpOptions);
};

// ------------------------------------------------------------------------------------------

/**
 * Raccourci requete DELETE
 * @param {String} callerId
 * @param {String} url
 */
HttpService.prototype.httpDelete = function (callerId, url) {

    return this.httpRequest(callerId, { method: 'DELETE', url });
};

// ------------------------------------------------------------------------------------------

/**
 * Raccourci requete GET
 * @param {String} callerId
 * @param {String} url
 */
HttpService.prototype.httpGet = function (callerId, url) {

    return this.httpRequest(callerId, { url });
};

// ------------------------------------------------------------------------------------------

/**
 * Raccourci requete POST
 * @param {String} callerId
 * @param {String} url
 * @param {Object} body
 */
HttpService.prototype.httpPost = function (callerId, url, body) {

    return this.httpRequest(callerId, { method: 'POST', url, body });
};

// ------------------------------------------------------------------------------------------

/**
 * Raccourci requete PUT
 * @param {String} callerId
 * @param {String} url
 * @param {Object} body
 */
HttpService.prototype.httpPut = function (callerId, url, body) {

    return this.httpRequest(callerId, { method: 'PUT', url, body });
};

// ------------------------------------------------------------------------------------------

module.exports = HttpService;
