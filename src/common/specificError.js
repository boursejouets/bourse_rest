'use strict';

/**
 * Création d'ojbets error pour différencier les erreurs techniques des erreurs fonctionnelles
 *
 */

var util = require('util');

function TechnicalError(message, extra) {
    if (!(this instanceof TechnicalError)) {
        return new TechnicalError(arguments);
    }
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message;
    this.extra = extra;

}

util.inherits(TechnicalError, Error);

function FunctionalError(message, extra) {
    if (!(this instanceof FunctionalError)) {
        return new FunctionalError(arguments);
    }

    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message;
    this.extra = extra;
}

util.inherits(FunctionalError, Error);

module.exports = {
    TechnicalError: TechnicalError,
    FunctionalError: FunctionalError
};
