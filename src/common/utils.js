'use strict';

const _ = require('lodash'),
            util = require('util'),
            moment = require('moment'),
            dateFormat = 'YYYY-MM-DD';

class Utils {

    /**
     * Retourne un objet period(beginDate, endDate) correspondant à la période précédente à la date passée en paramètre
     * (semaine précente, beginDate : lundi 00h00, endDate, lundi 00h00)
     *
     * @param {*} date
     */
    static previousPeriod(date) {
        let period = {};
        if (date && moment(date, dateFormat).isValid()) {
            // récupération du précédent lundi
            period.endDate = moment(date).startOf('isoweek').format(dateFormat);

            // récupération de l'avant dernier lundi
            period.startDate = moment(period.endDate).add(-1, 'days').startOf('isoweek').format(dateFormat);
        }

        return period;
    }

    /**
     * renvoie un array de storeId à partir d'un document json passé en entrée
     * [{ identifiant_magasin: '12106-10761' },
     *  { identifiant_magasin: '9661-8316' },
     *  { identifiant_magasin: '195278-195699' },
     *  { identifiant_magasin: '41316-39972' }];
     *
     * @param {*} clients
     */
    static clients2Array(clients) {
        let obj = clients.reduce(function(prev, curr){
            prev[curr.identifiant_magasin] = 1;
            return prev;
        },{});

        return Object.keys(obj);
    }

    /**
     * Formatage CSV d'une ligne de facturation Delubac
     */
    static createCSVLine() {
        return [].slice.call(arguments).join(';')+'\n';
    }

}

module.exports = Utils;
