'use strict';

module.exports = (opts, imports, register) => {

    const services = ['bourse'].reduce((s, name) => {
        s[name] = require('./' + name + '/' + name + 'Service')(s, imports, opts);
        return s;
    }, {});

    register(null, { services });
};

module.exports.consumes = ['log', 'mongo'];
module.exports.provides = ['services'];
