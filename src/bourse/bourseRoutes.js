'use strict';

const Router = require('../common/router');

module.exports = (imports) => {

    const rest = imports.rest;
    const bourseService = () => imports.services.bourse();

    // for preflight request
    rest.opts('/api/bourse/.*', require('cors')());

    rest.post('/api/bourse/:listId', (req, res) => {
        Router.handlePromise(bourseService().saveList(req.params.listId, req.body), req, res);
    });

    rest.opts('/api/bourse/cash/pay', require('cors')());
    rest.post('/api/bourse/cash/pay', (req, res) => {
        Router.handlePromise(bourseService().pay(req.body), req, res);
    });

    rest.get('/api/bourse/:listId', (req, res) => {
        Router.handlePromise(bourseService().getListById(req.params.listId), req, res);
    });

    rest.get('/api/bourse/:listId/product/:productId', (req, res) => {
        Router.handlePromise(bourseService().getProduct(req.params.listId, req.params.productId), req, res);
    });

};
