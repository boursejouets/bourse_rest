'use strict';

// const request = require('request-promise'),
//     requestCB = require('request'),
//     moment = require('moment'),
//     through2 = require('through2'),
//     deferred = require('deferred-stream'),
//     utils = require('../common/utils'),
//     invoicingRules = require('../repo/invoicingRules'),
//     specificError = require('../common/specificError');

// ---------------------------------------------------------------------------------------------------------------------

class BourseService {

    /**
     * @constructor
     * @param {Object} auth
     * @param {Object} services
     * @param {Object} imports
     * @param {Object} opts
     */
    constructor(services, imports, opts) {

        this.services = services;
        this.opts = opts;
        this.log = imports.log.getLogger('BourseService');
        this.mongo = imports.mongo.db;
        this.dbList = this.mongo.collection('listes');
    }

    // ---------------------------------------------------------------------------------------------------------------------
    async getListById(listId) {


        let result = {};
        try {
            result = await this.dbList.findOne({ id: listId });
            console.log(result);
        } catch (e) {
            this.log.error('[MONGO] dbfindList - findOne', e.message);
            throw e;
        }
        return result || {};
    }

    // ---------------------------------------------------------------------------------------------------------------------
    async preSaveList(listId, list) {

        this.log.debug('Sauvegrade list', JSON.stringify(list, null, 2));

        await this.dbList.update({ id: list.id },
            { $set: { products: list.products } },
            {
                upsert: true,
                multi: false
            });

        // await this.dbList.insert(list);

        return { save: 'OK' };
    }


    // ---------------------------------------------------------------------------------------------------------------------
    async saveList(listId, list) {

        this.log.debug('Sauvegrade list', JSON.stringify(list, null, 2));

        let save = { products: list.products };
        if (list.header) {
            save['header'] = list.header;
        }

        await this.dbList.update({ id: list.id },
            { $set: save },
            {
                upsert: true,
                multi: false
            });

        // await this.dbList.insert(list);

        return { save: 'OK' };
    }

    // ---------------------------------------------------------------------------------------------------------------------
    async getProduct(listId, productId) {
        let result = {};
        try {
            // db.listes.find({id:"741"}, {_id:0, id:1, "products.1":1});
            let prod = 'products.' + productId;
            let filter = { _id: 0, id: 1 };
            filter[prod] = 1;
            result = await this.dbList.findOne({ id: listId }, filter);
            console.log(result);
        } catch (e) {
            this.log.error('[MONGO] dbfindProduct - findOne', e.message);
            throw e;
        }
        return result || {};
    }

    async pay(list) {

        this.log.debug('Sauvegrade list', JSON.stringify(list, null, 2));
        await this.payForeach(Object.keys(list));
        return { save: 'OK' };
    }

    async payForeach(array) {
        for (let index = 0; index < array.length; index++) {
            await this.payOne(array[index]);
        }
    }

    async payOne(key) {

        let keys = key.split('-');
        let codelist = keys[0];
        let codeproduct = keys[1];
        let prod = 'products.' + codeproduct + '.sold';
        let setter = {};
        setter[prod] = true;

        await this.dbList.updateOne(
            {
                id: codelist,
            },
            {
                $set: setter
            },
            { upsert: false });
    }
}
// // ---------------------------------------------------------------------------------------------------------------------

module.exports = (services, imports, opts) => () => new BourseService(services, imports, opts);
