'use strict';

var httpErrors = require('restify-errors');

module.exports = (options, imports, register) => {

    function jsonBodyFilter(req, res, next) {

        if (req.body && typeof req.body == 'string') {
            imports.log.getLogger('App').info('incorrect string Content-Type so we try to convert in json.');
            try {
                req.body = JSON.parse(req.body);
            } catch (err) {
                imports.log.getLogger('App').error('json_body_filter - invalid json content : ', err);
                return next(new httpErrors.InvalidContentError('invalid json content'));
            }
        }
        next();
    }

    // Contrôle du flux json passé dans le body
    imports.rest.use(jsonBodyFilter);

    ['bourse'].forEach((name) => require('./' + name + '/' + name + 'Routes')(imports, options));

    register();
};

module.exports.consumes = ['rest', 'services', 'log'];
