'use strict';

const architect     = require('architect');

const config        = architect.loadConfig(__dirname+'/config.js');
const configRestify = config.find((cfg) => cfg.packagePath.indexOf('architect-restify') > -1);

architect.createApp(config, (err, app) => {

    if (err) throw err;

    const rest  = app.getService('rest');
    const log   = app.getService('log').getLogger('App');

    rest.on('uncaughtException', (req, res, route, err) => {
        log.fatal('Restify uncaughtException ->', err);
        // eslint-disable-next-line no-process-exit
        process.nextTick(() => process.exit());
    });

    ['unhandledRejection', 'uncaughtException'].forEach((event) => {
        process.on(event, (err) => {
            log.fatal(event, '->', err);
            // eslint-disable-next-line no-process-exit
            process.nextTick(() => process.exit());
        });
    });

    log.info('application started on port ' + configRestify.port);
});
