'use strict';

var bodyParser = require('restify-jsonstream-bodyparser');
const cors = require('cors');

module.exports = [
    {
        packagePath: 'architect-restify',
        port: 8081,
        plugins: {
            setcors: function () {
                return cors({
                    origin: true,
                    allowedHeaders: ['Origin', 'Accept', 'Accept-Version', 'Content-Length', 'Content-MD5', 'Content-Type',
                        'Date', 'x-web-token', 'x-bm-terminal', 'x-bm-date', 'X-CSRF-Token', 'Authorization']
                });
            },
            bodyParser: bodyParser
        }
    },
    {
        packagePath: 'architect-mongodb-native',
        url: 'mongodb://127.0.0.1/bourse'
    },
    {
        packagePath: 'architect-log4js',
        config: {
            appenders: {
                out: { type: 'stdout' },
                app: {
                    type: 'dateFile',
                    filename: '/tmp/log/bourse.log',
                    pattern: '-yyyy-MM-dd',
                    alwaysIncludePattern: false
                }
            },
            categories: {
                default: { appenders: ['app', 'out'], level: 'debug' }
            }
        }
    },
    {
        packagePath: './routes.js'
    },
    {
        packagePath: './services.js'
    },

];
