'use strict';

var invoicingRules = require('../../src/repo/invoicingRules'),
        moment = require('moment'),
        dateFormat = 'YYYY-MM-DD';

module.exports = {
    setUp: function (cb) {
        // DO NOTHING
        cb();
    },
    currentRules: {
        'test nominal': function (test) {

            let rule =  invoicingRules.currentRules();
            test.ok(rule);
            test.equal(0.02, rule.payback_bimedia.percentage);
            test.equal(null, rule.endDate);
            test.done();
        },
        'test date': function (test) {
            let rule =  invoicingRules.rulesByDate(moment().format(dateFormat));
            test.ok(rule);
            test.equal(0.02, rule.payback_bimedia.percentage);
            test.equal(null, rule.endDate);
            test.done();
        },
        'test non actif': function (test) {
            let rule =  invoicingRules.rulesByDate('2017-02-02');
            // on ne doit rien retourner
            test.ok(!rule);
            test.done();
        }
    }
};
