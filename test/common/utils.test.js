'use strict';

const utils = require('../../src/common/utils'),
            moment = require('moment'),
            dateFormat = 'YYYY-MM-DD',
            Ajv = require('ajv'),
            log4js = require('log4js'),
            assert = require('assert');

var logger;

before(() => {
    logger = log4js.getLogger();

    log4js.configure({
        appenders: {
            tuLogs: { type: 'file', filename: '/tmp/DedupTransformer.log' },
            console: { type: 'console' }
        },
        categories: {
            default: { appenders: ['console', 'tuLogs'], level: 'trace' }
        }
    });

    return;
});

describe('previousPeriod', () => {
    it('assert.nominal', () => {
        let date = moment('2018-04-03', dateFormat);
        let period = utils.previousPeriod(date);

        assert.ok(period.startDate && moment(period.startDate, 'YYYY-MM-DD').isValid(), 'format startDate invalid');
        assert.ok(period.endDate && moment(period.endDate, 'YYYY-MM-DD').isValid(), 'format endDate invalid');

        assert.equal(period.startDate, '2018-03-26');
        assert.equal(period.endDate, '2018-04-02');
    });

    it('lundi', () => {
        let date = moment('2018-05-14', dateFormat);
        let period = utils.previousPeriod(date);

        assert.equal(period.startDate, '2018-05-07');
        assert.equal(period.endDate, '2018-05-14');
    });

    it('dimanche', () => {
        let date = moment('2018-05-13', dateFormat);
        let period = utils.previousPeriod(date);

        assert.equal(period.startDate, '2018-04-30');
        assert.equal(period.endDate, '2018-05-07');
    });

    it('fin d année', () => {
        let date = moment('2020-01-08', dateFormat);
        let period = utils.previousPeriod(date);

        assert.equal(period.startDate, '2019-12-30');
        assert.equal(period.endDate, '2020-01-06');
    });

    it('bisextile', () => {
        let date = moment('2020-03-03', dateFormat);
        let period = utils.previousPeriod(date);

        assert.equal(period.startDate, '2020-02-24');
        assert.equal(period.endDate, '2020-03-02');

    });

});

describe('rounded', () => {
    it('nominal', () => {
        let number = 2.2599;
        let result = Number(number.toFixed(2));

        assert.equal('number', typeof result);
        assert.ok(2.26 === result);
    });

});

describe('weekNumber', () => {
    it('nominal', () => {
        let d = '2018-01-01';
        let weekNumber = moment(d, dateFormat).week();
        assert.equal(1, weekNumber);

        // lundi
        d = '2018-04-09';
        weekNumber = moment(d, dateFormat).week();
        assert.equal(15, weekNumber);

        // last day of year
        d = '2018-12-31';
        weekNumber = moment(d, dateFormat).week();
        assert.equal(1, weekNumber);
    });

});

describe('clients2Array', () => {
    it('nominal', () => {
        let clients = [{ identifiant_magasin: '12106-10761' },
            { identifiant_magasin: '9661-8316' },
            { identifiant_magasin: '195278-195699' },
            { identifiant_magasin: '9661-8316' },
            { identifiant_magasin: '41316-39972' }];

        let ar = utils.clients2Array(clients);
        assert.equal('4', ar.length);

        const storeId = '195278-195699';
        let found = ar.find(function (element) {
            return element === storeId;
        });
        assert.equal(storeId, found);
    });

});

describe('createCSVLine', () => {
    it('nominal', () => {
        let data = {
            _id: '12106-10761',
            total_depots: 40.5,
            nb_transac_cap_bas_pdv: 2,
            nb_transac_cap_haut_pdv: 2,
            nb_transac_percentage_pdv: 0,
            total_transac_percentage_pdv: 0,
            commission_pdv: 17,
            nb_transac_cap_bas_bimedia: 2,
            nb_transac_cap_haut_bimedia: 0,
            nb_transac_percentage_bimedia: 0,
            total_transac_percentage_bimedia: 0,
            commission_bimedia: 13
        };

        let line = utils.createCSVLine(data._id,
            data.total_depots,
            data.nb_transac_cap_bas_pdv,
            data.nb_transac_cap_haut_pdv,
            data.nb_transac_percentage_pdv,
            data.total_transac_percentage_pdv,
            data.commission_pdv);

        assert.equal('12106-10761;40.5;2;2;0;0;17\n', line);
    });

});

describe('SchemaJson', () => {
    it('Transaction', () => {
        let transactions = require('./sampleTx.json');
        let ajv = new Ajv();
        let schema = require('../../src/delubac/Transaction-schema.json');
        let validate = ajv.compile(schema);
        transactions.forEach(transac => {
            assert.ok(validate(transac));
            if (validate.errors) {
                // eslint-disable-next-line no-console
                console.log(validate.errors);
            }
        });
    });

});
