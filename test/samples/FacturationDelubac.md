==== Transaction à aggréger

db.transactions.aggregate([
        {$match:{
            'service_id':'DLB',
            'status.code':'OK',
            $and: [{ date: { $gte: new Date('2018-06-18') } }, { date: { $lt: new Date('2018-06-25') } }]
            }
        },
        {$project:{
            'store_id':1,
            'montant': '$product.unit_amount_ttc',
            'payback_pdv':'$product.payback.amount_pdv',
            'payback_bimedia':'$product.payback.amount_groupe_bimedia',
            'cap_bas_pdv': {$cond: [{$eq: ['$product.payback.amount_pdv', 2]}, 1, 0]},
            'cap_haut_pdv': {$cond: [{$eq: ['$product.payback.amount_pdv', 5]}, 1, 0]},
            'percentage_pdv': {$cond: [{$and :[ {$gt: ['$product.payback.amount_pdv', 2]}, {$lt: ['$product.payback.amount_pdv', 5]}]}, 1, 0]},
            'sum_percentage_pdv': {$cond: [{$and :[ {$gt: ['$product.payback.amount_pdv', 2]}, {$lt: ['$product.payback.amount_pdv', 5]}]}, '$montant', 0]},
            'cap_bas_bimedia': {$cond: [{$eq: ['$product.payback.amount_groupe_bimedia', 3]}, 1, 0]},
            'cap_haut_bimedia': {$cond: [{$eq: ['$product.payback.amount_groupe_bimedia', 10]}, 1, 0]},
            'percentage_bimedia': {$cond: [{$and :[ {$gt: ['$product.payback.amount_groupe_bimedia', 3]}, {$lt: ['$product.payback.amount_groupe_bimedia', 10]}]}, 1, 0]},
            'sum_percentage_bimedia': {$cond: [{$and :[ {$gt: ['$product.payback.amount_groupe_bimedia', 3]}, {$lt: ['$product.payback.amount_groupe_bimedia', 10]}]}, '$montant', 0]}
        }},
        {$group:{
            _id : '$store_id',
            'total_depots' : { $sum : '$montant' },
            'nb_transac_cap_bas_pdv' : { $sum : '$cap_bas_pdv' },
            'nb_transac_cap_haut_pdv' : { $sum : '$cap_haut_pdv' },
            'nb_transac_percentage_pdv' : { $sum : '$percentage_pdv' },
            'total_transac_percentage_pdv' : { $sum : '$sum_percentage_pdv' },
            'commission_pdv': { $sum : '$payback_pdv' },
            'nb_transac_cap_bas_bimedia' : { $sum : '$cap_bas_bimedia' },
            'nb_transac_cap_haut_bimedia' : { $sum : '$cap_haut_bimedia' },
            'nb_transac_percentage_bimedia' : { $sum : '$percentage_bimedia' },
            'total_transac_percentage_bimedia' : { $sum : '$sum_percentage_bimedia' },
            'commission_bimedia': { $sum : '$payback_bimedia' },
        }},
        {$project:{
            _id:1,
            'total_depots': 1,
            'nb_transac_cap_bas_pdv': 1,
            'nb_transac_cap_haut_pdv': 1,
            'nb_transac_percentage_pdv': 1,
            'total_transac_percentage_pdv': 1,
            'commission_pdv' : {$cond: [{$lt: ['$commission_pdv', 10]}, 10, '$commission_pdv']},
            'nb_transac_cap_bas_bimedia': 1,
            'nb_transac_cap_haut_bimedia': 1,
            'nb_transac_percentage_bimedia': 1,
            'total_transac_percentage_bimedia': 1,
            'commission_bimedia' : {$cond: [{$lt: ['$commission_bimedia', 13]}, 13, '$commission_bimedia']}
        }}
])

=== récupération des fichiers zip résumé de facturation
wget --http-user='virtual' --http-password='IrVe1P#_4FtBk}7' --content-disposition 'https://services.bimedia-it.com/delubac/api/private/invoice/summary?date=2018-06-11'
wget --http-user='virtual' --http-password='IrVe1P#_4FtBk}7' --content-disposition 'https://services.bimedia-it.com/delubac/api/private/invoice/summary'


=== Mise à jour d'une transaction - structure des frais ===

db.transactions.update(
        {service_id:'DLB', transaction_id : 'DLB-71'},
        { 
            $set: { 'product.payback.amount_groupe_bimedia': 4.6,
                    'product.payback.amount_pdv': 2.3,
                    'product.fees': 0
            },
            $unset: { 'product.discount_rate': "",
                      'product.fees_discount_rate': ""
            }
        },
        {
            upsert: false,
            multi: false
        });

=== Mise à jour d'une transaction - informations de facturation ===

db.transactions.update(
        {service_id:'DLB', transaction_id : 'DLB-28'},
        { 
            $set: { invoices: [
                {
                    number:'FC18SMP00120238',
                    date_invoice: new Date('2018-05-18'),
                    date_creation: new Date('2018-06-05')
                }
            ]}
        },
        {
            upsert: false,
            multi: false
        });


=== migration Delubac v1.0.2 vers v1.1.2 ===

db.transactions.find({ service_id: 'DLB' }).forEach(function (tx) {
    tx.date = new Date(tx.date);
    db.transactions.save(tx);
});

