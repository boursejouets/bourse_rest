'use strict';

const mockery = require('mockery'),
    moment = require('moment'),
    log4js = require('log4js'),
    assert = require('assert');

var auth = require('./auth.json');
var services = {};
var mongo = {};
var opts = require('./delubacService_opt.json');

var logger;

before(() => {

    // Mock Logger
    log4js.configure({
        appenders: {
            tuLogs: { type: 'file', filename: '/tmp/DedupTransformer.log' },
            console: { type: 'console' }
        },
        categories: {
            default: { appenders: ['console', 'tuLogs'], level: 'trace' }
        }
    });
    logger = log4js.getLogger('Mocha Delubac Service');

    // Mock auth
    auth.created = moment().format();
    auth.lastusage = moment().format();

    // Mock Mongo
    // TODO
    mongo = { db: { collection: function (collection) { return 'lapin'; } } };

    mockery.enable({
        warnOnReplace: true,
        warnOnUnregistered: false,
        useCleanCache: true
    });

    return;
});

describe('', () => {

    let service;

    beforeEach(() => {

        mockery.registerMock('request-promise', function (options) {

            console.log(JSON.stringify(options, null, 2));

            var response = require('./responseDelubac-getPosInfos.json');
            return Promise.resolve(JSON.stringify(response));
        });
    });

    it('getPosInfos', (done) => {
        var dblService = require('../../src/delubac/delubacService');
        let imports = { log: log4js, mongo: mongo };
        service = new dblService.DelubacService(auth, services, imports, opts);

        service.getPosInfos('16567-15222', 'monToken').then(data => {
            assert.equal(data.response.net_ref, '16567-15222');
            assert.equal(data.response.zipcode, '13016');
            assert.equal(data.response.is_active, true);
            assert.equal(Number(data.response.deposit_limit), 20000);
            done();
        }).catch(error => {
            return done(new Error(error.message));
        });
    });

    afterEach(() => {
        mockery.deregisterMock('request-promise');
    });

});

after(() => {
    mockery.disable();
    mockery.deregisterAll();
});
